import sqlalchemy as db
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from config import db_name


engine = db.create_engine(f"sqlite:///{db_name}", echo=True, connect_args={"check_same_thread": False})
session = sessionmaker(bind=engine)
s = session()
Base = declarative_base()


def run_db():
    Base.metadata.create_all(engine)


def add_new_task(user_id, text, date_time):
    new_task = Tasks(user_id=user_id, text=text, date_time=date_time)
    s.add(new_task)
    s.commit()
    s.refresh(new_task)
    task_id = new_task.task_id
    return task_id


def redact_task(task_id, text):
    task = s.query(Tasks).filter(Tasks.task_id == task_id).one()
    task.text = text
    s.flush()
    s.commit()
    return 'task_redacted'


def get_task_info(task_id):
    task = s.query(Tasks).filter(Tasks.task_id == task_id).one()
    return task.text, task.user_id


def delete_task(task_id):
    task = s.query(Tasks).filter(Tasks.task_id == task_id).one()
    s.delete(task)
    s.commit()
    return 'task_deleted'


def show_tasks(user_id):
    task_id_list = []
    tasks = s.query(Tasks).filter(Tasks.user_id == user_id).all()
    result = '*Номер | Задача | Время (в порядке поступления)*\n\n'
    if len(tasks) == 0:
        return 'Задач нет!'
    for i in range(len(tasks)):
        result += f'{i+1} | {tasks[i].text} | {tasks[i].date_time}\n'
        task_id_list.append(tasks[i].task_id)
    return result, task_id_list


def check_mailing_status(user_id):
    record = s.query(Mailing).filter(Mailing.user_id == user_id).first()
    if record is None:
        return 'mailing_off'
    else:
        return 'mailing_on'


def add_user_to_mailing(user_id):
    record = Mailing(user_id=user_id)
    s.add(record)
    s.commit()
    return 'user_added'


def delete_user_from_mailing(user_id):
    record = s.query(Mailing).filter(Mailing.user_id == user_id).first()
    s.delete(record)
    s.commit()
    return 'user_deleted'


def get_mailing_users():
    users = s.query(Mailing).all()
    answer = []
    for user in users:
        answer.append(user.user_id)
    return answer


class Tasks(Base):
    __tablename__ = 'Tasks'

    task_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.String, nullable=False)
    text = db.Column(db.String)
    date_time = db.Column(db.DateTime)


class Mailing(Base):
    __tablename__ = 'Mailing'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)


run_db()
