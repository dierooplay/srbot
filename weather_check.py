from datetime import datetime
from requests import get

key = 'ваш_ключ_openweathermap'


def get_weather(loc):
    if type(loc) == tuple:
        url = f'http://api.openweathermap.org/data/2.5/weather?lat={loc[0]}&lon={loc[1]}&appid={key}&units=metric'
    else:
        url = f'http://api.openweathermap.org/data/2.5/weather?q=Moscow&appid={key}&units=metric'
    raw_data = get(url)
    if raw_data.status_code != 200:
        return 'not_200'
    else:
        data = raw_data.json()
        curr_time = datetime.fromtimestamp(data['dt'])
        curr_time = ("{:%H:%M:%S %d.%m.%Y}".format(curr_time))
        temperature = data['main']['temp']
        feels_like = data['main']['feels_like']
        wind = data['wind']['speed']
        humidity = data['main']['humidity']
        return curr_time, temperature, feels_like, wind, humidity
