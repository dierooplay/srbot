import telebot
from apscheduler.schedulers.background import BackgroundScheduler
from datetime import datetime


from config import bot_token
from weather_check import get_weather
from database import *

bot = telebot.TeleBot(bot_token)


@bot.message_handler(commands=['start'])
def start(message):
    keyboard = telebot.types.InlineKeyboardMarkup()
    keyboard.add(telebot.types.InlineKeyboardButton(text='Меню', callback_data='menu'))
    bot.send_message(message.chat.id, f"Привет, {message.from_user.first_name}!\n\n"
                                      "Я - бот, который напомнит тебе что-нибудь сделать!\n\n"
                                      "Создать задачу - /task\n"
                                      "Меню - /menu\n"
                                      "Создатель - https://t.me/gsimoon", disable_web_page_preview=True,
                     reply_markup=keyboard)


@bot.message_handler(commands=['menu'])
def mess_menu(message):
    keyboard = telebot.types.InlineKeyboardMarkup()
    keyboard.add(telebot.types.InlineKeyboardButton(text='Прогноз погоды', callback_data='weather_check'),
                 telebot.types.InlineKeyboardButton(text='Список задач', callback_data='tasklist'))
    keyboard.add(telebot.types.InlineKeyboardButton(text='Ежедневная сводка', callback_data='schedule'),
                 telebot.types.InlineKeyboardButton(text='Создать задачу', callback_data='create_task'))
    bot.send_message(message.chat.id, "Вывожу команды...\n\n"
                                      "/weather - прогноз погоды по геолокации\n"
                                      "/task - создание напоминания\n"
                                      "/schedule - ежедневная сводка\n"
                                      "/tasklist - список задач", reply_markup=keyboard)


@bot.message_handler(commands=['weather'])
def weather(message):
    bot.send_message(message.chat.id, 'Я могу показать погоду, если ты отправишь мне геолокацию!\n\n'
                                      'Нажми на скрепку -> "Location"\n-> Send My Current Location\n'
                                      'Только убедись, что у Telegram есть доступ к твоей локации!')


@bot.message_handler(commands=['task'])
def task_creation(message):
    keyboard = telebot.types.InlineKeyboardMarkup()
    keyboard.add(telebot.types.InlineKeyboardButton(text='Отмена', callback_data='menu'))
    msg = bot.send_message(message.chat.id, 'Создаём задачу!\n'
                                            'Что нужно напомнить?', reply_markup=keyboard)
    bot.register_next_step_handler(msg, wait_for_task_text)


def wait_for_task_text(message):
    text = message.text
    msg = bot.send_message(message.chat.id, 'Запомнил, теперь напишите время, когда необходимо напомнить!\n'
                                            '(В формате ДД.ММ.ГГГГ ЧЧ:ММ, например, 22.02.2022 13:30)')
    bot.register_next_step_handler(msg, wait_for_task_time, text)


def wait_for_task_time(message, text):
    time_str = message.text
    try:
        time = datetime.strptime(time_str, "%d.%m.%Y %H:%M")
        if (str(time-datetime.now()))[0] == '-':
            keyboard = telebot.types.InlineKeyboardMarkup()
            keyboard.add(telebot.types.InlineKeyboardButton(text='Отмена', callback_data='menu'))
            msg = bot.send_message(message.chat.id, 'Это время уже в прошлом! Введите ещё раз.', reply_markup=keyboard)
            bot.register_next_step_handler(msg, wait_for_task_time, text)
        else:
            bot.send_message(message.chat.id, 'Принято!\n'
                                              f'Напоминание: {text}\n'
                                              f'Время: {time_str}')
            task_id = add_new_task(str(message.chat.id), text, time)
            scheduler.add_job(remind_message, 'date', args=[task_id], run_date=time)
    except ValueError:
        keyboard = telebot.types.InlineKeyboardMarkup()
        keyboard.add(telebot.types.InlineKeyboardButton(text='Отмена', callback_data='menu'))
        msg = bot.send_message(message.chat.id, 'Неверный формат времени! Попробуйте ещё раз.', reply_markup=keyboard)
        bot.register_next_step_handler(msg, wait_for_task_time, text)


@bot.message_handler(commands=['tasklist'])
def tasklist(message):
    if show_tasks(message.chat.id) == 'Задач нет!':
        bot.send_message(message.chat.id, 'Задач нет!')
    else:
        keyboard = telebot.types.InlineKeyboardMarkup()
        keyboard.add(telebot.types.InlineKeyboardButton(text='Изменить текст задачи', callback_data='change_task_text'))
        tasks, id_list = show_tasks(message.chat.id)
        bot.send_message(message.chat.id, tasks, parse_mode='Markdown', reply_markup=keyboard)


def change_task_text(message):
    task_id_list = (show_tasks(message.chat.id))[1]
    msg = bot.send_message(message.chat.id, 'Введите номер задачи, текст которой хотите изменить')
    bot.register_next_step_handler(msg, get_task_number, task_id_list)


def get_task_number(message, task_id_list):
    task_number = message.text
    if int(task_number) > len(task_id_list) or int(task_number) <= 0:
        keyboard = telebot.types.InlineKeyboardMarkup()
        keyboard.add(telebot.types.InlineKeyboardButton(text='Отмена', callback_data='menu'))
        msg = bot.send_message(message.chat.id, 'Задачи под таким номером не существует!\nВведите корректный номер.',
                               reply_markup=keyboard)
        bot.register_next_step_handler(msg, get_task_number, task_id_list)
    else:
        task_id = task_id_list[(int(task_number))-1]
        msg = bot.send_message(message.chat.id, 'Задача выбрана!\nТеперь введите новый текст.')
        bot.register_next_step_handler(msg, redact_task_text, task_id)


def redact_task_text(message, task_id):
    new_text = message.text
    if redact_task(task_id, new_text) == 'task_redacted':
        bot.send_message(message.chat.id, 'Текст задачи обновлён!')


@bot.message_handler(commands=['schedule'])
def schedule(message):
    if check_mailing_status(message.chat.id) == 'mailing_off':
        keyboard = telebot.types.InlineKeyboardMarkup()
        keyboard.add(telebot.types.InlineKeyboardButton(text='Включить сводку', callback_data='try_schedule'))
        bot.send_message(message.chat.id, 'Я умею составлять утренние сводки!\n\n'
                                          'Каждый день в 8:00 я присылаю погоду и список задач на день!\n',
                         reply_markup=keyboard)
    else:
        keyboard = telebot.types.InlineKeyboardMarkup()
        keyboard.add(telebot.types.InlineKeyboardButton(text='Отключить сводку', callback_data='stop_schedule'))
        bot.send_message(message.chat.id, 'Напоминания работают', reply_markup=keyboard)


@bot.message_handler(content_types=['location'])
def location(message):
    if message.location is not None:
        loc = (message.location.latitude, message.location.longitude)
        if get_weather(loc) == 'not_200':
            bot.send_message(message.chat.id, 'Сервис с погодой недоступен')
        else:
            curr_time, temperature, feels_like, wind, humidity = get_weather(loc)
            bot.send_message(message.chat.id, f'Погода на {curr_time}:\n'
                                              f'Температура: {round(temperature)}°C,'
                                              f' ощущается как {round(feels_like)}°C\n'
                                              f'Скорость ветра: {wind} м/c\n'
                                              f'Влажность: {humidity}%\n'
                                              'Хорошего дня!')


@bot.callback_query_handler(func=lambda call: True)
def button_react(call):
    if call.data == 'create_task':
        task_creation(call.message)
    if call.data == 'change_task_text':
        change_task_text(call.message)
        bot.edit_message_reply_markup(call.message.chat.id, call.message.id, reply_markup=None)
    if call.data == 'weather_check':
        weather(call.message)
    if call.data == 'schedule':
        schedule(call.message)
    if call.data == 'try_schedule':
        bot.send_message(call.message.chat.id, 'Сводка включена!')
        bot.edit_message_reply_markup(call.message.chat.id, call.message.id, reply_markup=None)
        add_user_to_mailing(call.message.chat.id)
    if call.data == 'stop_schedule':
        bot.send_message(call.message.chat.id, 'Сводка выключена!')
        bot.edit_message_reply_markup(call.message.chat.id, call.message.id, reply_markup=None)
        delete_user_from_mailing(call.message.chat.id)
    if call.data == 'tasklist':
        tasklist(call.message)
    if call.data == 'menu':
        mess_menu(call.message)


def remind_message(task_id):
    text, user_id = get_task_info(task_id)
    bot.send_message(user_id, f'*Напоминание!*\n{text}', parse_mode='Markdown')
    delete_task(task_id)


def every_day_message():
    for user in get_mailing_users():
        tasks = show_tasks(user)
        bot.send_message(user, 'Доброе утро! Как спалось?\n')
        curr_time, temperature, feels_like, wind, humidity = get_weather(None)
        bot.send_message(user, f'Погода на {curr_time}:\n'
                            f'Температура: {round(temperature)} °C, ощущается как {round(feels_like)} °C\n'
                            f'Скорость ветра: {wind} м/c\n'
                            f'Влажность: {humidity}%\n'
                            'Хорошего дня!')
        bot.send_message(user, tasks, parse_mode='Markdown')


if __name__ == "__main__":
    print('{:%H:%M:%S %d.%m.%Y}'.format(datetime.now()), '- Бот запущен')
    while True:
        try:
            scheduler = BackgroundScheduler()
            scheduler.add_job(every_day_message, 'cron', hour=8)
            scheduler.start()

            bot.polling(non_stop=True)
            run_db()

        except Exception as e:
            print(e)
            pass
